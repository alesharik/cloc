#ifndef CLOC_DATE_FILE_PREPROCESSOR_H
#define CLOC_DATE_FILE_PREPROCESSOR_H

#include "file_preprocessor.h"
#include "cache.h"

class DateFilePreprocessor : public FilePreprocessor {
    boost::filesystem::path path;
    std::shared_ptr<Cache> cache;
public:
    DateFilePreprocessor(const boost::filesystem::path path, std::shared_ptr<Cache> cache);

    void preprocess_file() override;

    bool is_file_already_processed() override;

    void finish_processing() override;
};

class DateFilePreprocessorFactory : public FilePreprocessorFactory {
    std::shared_ptr<Cache> cache;
public:
    explicit DateFilePreprocessorFactory(std::shared_ptr<Cache> cache);

    std::unique_ptr<FilePreprocessor> create(const boost::filesystem::path path) override;
};

#endif //CLOC_DATE_FILE_PREPROCESSOR_H
