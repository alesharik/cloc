#ifndef CLOC_FIBER_POOL_H
#define CLOC_FIBER_POOL_H

#include <boost/fiber/all.hpp>
#include <boost/atomic.hpp>
#include <vector>
#include <thread>

namespace {
    class FiberPoolManager {
        std::vector<std::thread *> workers = {};
        boost::fibers::mutex shutdown_mutex = {};
        boost::fibers::condition_variable_any shutdown_trigger = {};
        std::thread *main = nullptr;
        boost::atomic_int fibers;
    public:
        virtual ~FiberPoolManager();

        void start(unsigned int concurrency, unsigned int actualConcurrency);

        void shutdown();

        inline void increment_fibers() {
            fibers++;
        }

        inline void decrement_fibers() {
            fibers--;
        }
    };
}

class FiberPool {
    static FiberPoolManager manager;
public:
    static void start();

    static void start(unsigned int concurrency);

    static void shutdown();

    inline static void acknowledge_new_fiber() {
        manager.increment_fibers();
    }

    inline static void acknowledge_fiber_dead() {
        manager.decrement_fibers();
    }
};

#endif //CLOC_FIBER_POOL_H
