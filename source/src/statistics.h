#ifndef CLOC_STATISTICS_H
#define CLOC_STATISTICS_H

#include <iostream>

struct FileStatistics {
    int physicalLines;
    int logicalLines;
    int commentLines;
    int blankLines;
    int precompilerExpressions;
    static const int size = sizeof(FileStatistics::physicalLines) + sizeof(FileStatistics::logicalLines) +
                            sizeof(FileStatistics::blankLines) +
                            sizeof(FileStatistics::commentLines) + sizeof(precompilerExpressions);

    FileStatistics();

    explicit FileStatistics(FileStatistics *cpy);

    ///Backwards compatibility
    FileStatistics(int physicalLines, int logicalLines, int commentLines, int blankLines);

    FileStatistics(int physicalLines, int logicalLines, int commentLines, int blankLines, int precompilerExpressions);

    bool operator==(const FileStatistics &other) const;

    void operator+=(const FileStatistics &other);
};

std::istream &operator>>(std::istream &stream, FileStatistics &stats);

std::ostream &operator<<(std::ostream &stream, const FileStatistics &stats);

#endif //CLOC_STATISTICS_H
