#include "checksum_file_preprocessor.h"
#include "async_file.h"
#include <boost/fiber/buffered_channel.hpp>

ChecksumFilePreprocessor::ChecksumFilePreprocessor(const boost::filesystem::path path, std::shared_ptr<Cache> cache,
                                                   std::unique_ptr<Hash, HashDeleter> hash) : path(path),
                                                                                              cache(std::move(cache)),
                                                                                              hash(std::move(hash)),
                                                                                              checksum("") {

}

void ChecksumFilePreprocessor::preprocess_file() {
    hash->init();
    AsyncFile file(path);
    auto channel = make_shared<boost::fibers::unbuffered_channel<file_part>>();
    file.open(channel);
    file_part part;
    while (boost::fibers::channel_op_status::success == channel->pop(std::ref(part))) {
        hash->update(part.data, static_cast<unsigned int>(part.length));
        delete[] part.data;
    }
    checksum = hash->final();
}

bool ChecksumFilePreprocessor::is_file_already_processed() {
    return cache->get_checksum(path) == checksum;
}

void ChecksumFilePreprocessor::finish_processing() {
    if (checksum.empty())
        return;
    cache->put_checksum(path, checksum);
}

ChecksumFilePreprocessor::~ChecksumFilePreprocessor() {
    hash = nullptr;
}

ChecksumFileProcessorFactory::ChecksumFileProcessorFactory(std::shared_ptr<Cache> cache,
                                                           std::shared_ptr<Hasher> hasher) : cache(std::move(cache)),
                                                                                             hasher(std::move(hasher)) {

}

std::unique_ptr<FilePreprocessor> ChecksumFileProcessorFactory::create(const boost::filesystem::path path) {
    return std::make_unique<ChecksumFilePreprocessor>(path, cache,
                                                      std::unique_ptr<Hash, HashDeleter>(hasher->newHash()));
}
