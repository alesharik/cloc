#ifndef CLOC_CHARSET_DECODER_H
#define CLOC_CHARSET_DECODER_H

#include <enca.h>
#include <string>

class CharsetDecoder {
    static std::string language;
public:
    static void init(const std::string &lang);

    /// Analyze given file part and return guessed encoding
    /// \param data file part data
    /// \param length part's length
    /// \param endOfFile given part is whole file
    /// \return encoding name. Empty string will be returned if encoding cannot be guessed
    static std::string decode(const char *data, size_t length, bool endOfFile);
};


#endif //CLOC_CHARSET_DECODER_H
