#ifndef CLOC_CHECKSUM_FILE_PREPROCESSOR_H
#define CLOC_CHECKSUM_FILE_PREPROCESSOR_H

#include "file_preprocessor.h"
#include "cache.h"
#include "hashing.h"

class ChecksumFilePreprocessor : public FilePreprocessor {
    boost::filesystem::path path;
    std::shared_ptr<Cache> cache;
    std::unique_ptr<Hash, HashDeleter> hash;
    std::string checksum;
public:
    ChecksumFilePreprocessor(const boost::filesystem::path path, std::shared_ptr<Cache> cache,
                             std::unique_ptr<Hash, HashDeleter> hash);

    ~ChecksumFilePreprocessor() override;

    void preprocess_file() override;

    bool is_file_already_processed() override;

    void finish_processing() override;
};

class ChecksumFileProcessorFactory : public FilePreprocessorFactory {
    std::shared_ptr<Cache> cache;
    std::shared_ptr<Hasher> hasher;
public:
    explicit ChecksumFileProcessorFactory(std::shared_ptr<Cache> cache, std::shared_ptr<Hasher> hasher);

    std::unique_ptr<FilePreprocessor> create(boost::filesystem::path path) override;
};


#endif //CLOC_CHECKSUM_FILE_PREPROCESSOR_H
