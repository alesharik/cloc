#ifndef CLOC_LINE_COUNTER_ALGORITHM_MANAGER_H
#define CLOC_LINE_COUNTER_ALGORITHM_MANAGER_H

#include <string>
#include <map>
#include "line_counter_algorithm.h"

class LineCounterAlgorithmManager {
    static std::map<std::string, LineCounterAlgorithm *> algorithms;
public:
    static void init();

    static bool support_file_extension(std::string extension);

    static LineCounterAlgorithm *get_for_file_extension(std::string extension);
};

#endif //CLOC_LINE_COUNTER_ALGORITHM_MANAGER_H
