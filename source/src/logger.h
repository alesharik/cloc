#ifndef CLOC_LOGGER_H
#define CLOC_LOGGER_H

#include <boost/log/common.hpp>
#include <boost/log/sources/severity_logger.hpp>
#include <boost/log/attributes.hpp>
#include <stdexcept>
#include <iostream>

enum severity_level {
    debug,
    info,
    warn,
    warning,
    error
};

template<typename CharT, typename TraitsT>
inline std::basic_ostream<CharT, TraitsT> &operator<<(
        std::basic_ostream<CharT, TraitsT> &strm, severity_level level) {
    switch (level) {
        case debug:
            strm << "debug";
            break;
        case info:
            strm << "info";
            break;
        case warn:
            strm << "warn";
            break;
        case warning:
            strm << "warning";
            break;
        case error:
            strm << "error";
            break;
        default:
            throw std::runtime_error("Severity level " + std::to_string(level) + " not found");
    }
    return strm;
}

BOOST_LOG_INLINE_GLOBAL_LOGGER_DEFAULT(logger, boost::log::sources::severity_logger_mt<severity_level>)

class Logger {
public:
    static void init();

    static boost::log::sources::severity_logger_mt<severity_level> get_logger(std::string tag);

    static void set_severity_level(severity_level level);
};

#endif //CLOC_LOGGER_H
