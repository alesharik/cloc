#include "charset_converter.h"
#include <iconv.h>
#include <memory.h>

std::string CharsetConverter::to_utf8(const char *input, size_t length, const std::string &charset) {
    char *in = new char[length];
    memcpy(in, input, length);
    iconv_t conv = iconv_open("UTF-8", charset.c_str());
    char *out = new char[length * 2];
    char *pout = out;
    size_t dstLen = length * 2;
    iconv(conv, &in, &length, &pout, &dstLen);
    iconv_close(conv);
    std::string ret(out, dstLen);
    delete[] out;
    return ret;
}
