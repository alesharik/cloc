#include "logger.h"
#include <boost/core/null_deleter.hpp>
#include <boost/log/core.hpp>
#include <boost/log/sinks.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/date_time/posix_time/ptime.hpp>
#include <boost/log/trivial.hpp>

using namespace boost::log;

BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", severity_level)
BOOST_LOG_ATTRIBUTE_KEYWORD(tag_attr, "Tag", std::string)

void Logger::init() {
    auto core = core::get();
    core->remove_all_sinks();
    core->reset_filter();
    core->add_global_attribute("TimeStamp", attributes::local_clock());
    core->add_global_attribute("Severity", attributes::constant<severity_level>(severity_level::info));
    core->add_global_attribute("Tag", attributes::constant<std::string>("None"));

    auto sink = boost::make_shared<sinks::synchronous_sink<sinks::text_ostream_backend>>();
    sink->locked_backend()->add_stream(boost::shared_ptr<std::ostream>(&std::clog, boost::null_deleter()));
    sink->locked_backend()->auto_flush(true);
    sink->set_formatter(expressions::stream
                                << "[" << expressions::format_date_time<boost::posix_time::ptime>("TimeStamp",
                                                                                                  "%Y-%m-%d %H:%M:%S")
                                << "] "
                                << "[" << severity << "] "
                                << "[" << tag_attr << "] "
                                << expressions::message);
    core->add_sink(sink);
    set_severity_level(info);
}

sources::severity_logger_mt<severity_level> Logger::get_logger(const std::string tag) {
    auto l = logger::get();
    l.add_attribute("Tag", attributes::constant<std::string>(tag));
    return l;
}

void Logger::set_severity_level(severity_level level) {
    core::get()->set_filter(severity >= level);
}