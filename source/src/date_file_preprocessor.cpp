#include "date_file_preprocessor.h"

DateFilePreprocessor::DateFilePreprocessor(const boost::filesystem::path path, std::shared_ptr<Cache> cache) : path(
        std::move(path)), cache(std::move(cache)) {
}

void DateFilePreprocessor::preprocess_file() {
}

bool DateFilePreprocessor::is_file_already_processed() {
    return cache->get_change_time(path) == boost::posix_time::from_time_t(boost::filesystem::last_write_time(path));
}

void DateFilePreprocessor::finish_processing() {
    cache->put_change_time(path, boost::posix_time::from_time_t(boost::filesystem::last_write_time(path)));
}

DateFilePreprocessorFactory::DateFilePreprocessorFactory(std::shared_ptr<Cache> cache) : cache(std::move(cache)) {
}

std::unique_ptr<FilePreprocessor> DateFilePreprocessorFactory::create(const boost::filesystem::path path) {
    return std::make_unique<DateFilePreprocessor>(path, cache);
}