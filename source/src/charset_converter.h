#ifndef CLOC_CHARSET_CONVERTER_H
#define CLOC_CHARSET_CONVERTER_H

#include <string>

class CharsetConverter {
public:
    static std::string to_utf8(const char *input, size_t length, const std::string &charset);
};


#endif //CLOC_CHARSET_CONVERTER_H
