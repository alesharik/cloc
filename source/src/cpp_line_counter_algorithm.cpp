#include "cpp_line_counter_algorithm.h"
#include <deque>
#include <boost/regex.hpp>

using namespace boost;

inline bool ends_with(std::string const &value, std::string const &ending) {
    if (ending.size() > value.size())
        return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}

FileStatistics CppLineCounterAlgorithm::count(std::string file) {
    FileStatistics stats;
    bool isBlankLine = true;
    bool isCommentLine = false;
    bool isCommentBlock = false;
    bool isString = false;
    bool isRawString = false;
    bool isPrecompilerExpression = false;
    std::deque<state> states;
    states.push_front(state{NONE});
    for (int i = 0; i < file.size(); i++) {
        char c = file[i];
        //Physical line counter:
        if (c == '\n')
            stats.physicalLines++;

        //Blank line counter:
        if (c == '\n') {
            if (isBlankLine)
                stats.blankLines++;
            isBlankLine = true;
        } else if (c != ' ' && c != '\t')
            isBlankLine = false;

        //Strings:
        if (c == '"' && file[i - 1] == 'R' && (file.size() > i + 1 && file[i + 1] == '('))
            isRawString = true;
        if (isRawString && c == '"' && file[i - 1] == ')')
            isRawString = false;
        if (isRawString)
            continue;

        if (c == '"' && file[i - 1] != '\\')
            isString = !isString;
        if (isString || isRawString)
            continue;

        //Comment lines:
        if (c == '/' && (file.size() > i + 1 && file[i + 1] == '/')) {
            isCommentLine = true;
            stats.commentLines++;
        }
        if (isCommentLine && c == '\n')
            isCommentLine = false;

        //Comment blocks:
        if (isCommentBlock && c == '*' && (file.size() > i + 1 && file[i + 1] == '/'))
            isCommentBlock = false;
        if (c == '/' && (file.size() > i + 1 && file[i + 1] == '*')) {
            isCommentBlock = true;
            stats.commentLines++;
        }
        if (c == '\n' && isCommentBlock)
            stats.commentLines++;

        //Skip comments:
        if (isCommentLine || isCommentBlock)
            continue;

        //Precompiler expressions:
        if (c == '#') {
            isPrecompilerExpression = true;
            stats.precompilerExpressions++;
        }
        if (isPrecompilerExpression) {
            if (c == '\n')
                isPrecompilerExpression = false;
            continue;
        }

        //MAIN:
        if (states.front().block == NONE) {
            states.front().buffer += c;
            if (c == ';') {
                bool seemsEmpty = true;
                for (char &q : states.front().buffer) {
                    if ((q >= 'a' && q <= 'z') || (q >= 'A' && q <= 'Z') || (q >= '0' && q <= '9')) {
                        seemsEmpty = false;
                        break;
                    }
                }
                if (!seemsEmpty)
                    stats.logicalLines++;
                states.front().buffer = "";
            }
            if (c == '{') {
                regex classCheckRegex(R"(class|struct .*?{)");
                regex functionCheckRegex(R"(.*? .*?\(.*?\).*?{)");
                if (regex_search(states.front().buffer, classCheckRegex))
                    states.push_front(state{STRUCT});
                else if (regex_search(states.front().buffer, functionCheckRegex))
                    states.push_front(state{FUNCTION});
                else
                    states.push_front(state{BLOCK});
                stats.logicalLines++;
                continue;
            }
        } else if (states.front().block == STRUCT) {
            states.front().buffer += c;
            regex classCheckRegex(R"(class|struct .*?{)");
            regex functionCheckRegex(R"(.*? .*?\(.*?\).*?{)");
            if (regex_search(states.front().buffer, classCheckRegex))
                states.push_front(state{STRUCT});
            else if (regex_search(states.front().buffer, functionCheckRegex))
                states.push_front(state{FUNCTION});
            else {
                if (c == ';') {
                    if (ends_with(states.front().buffer, "}"))
                        states.pop_front();
                    else {
                        stats.logicalLines++;
                        states.front().buffer = "";
                    }
                }
            }
        } else if (states.front().block == FUNCTION) {
            states.front().buffer += c;
            regex controlConstructionCheck(
                    R"(if\W*?\(.*?\)|do|else|while\W*?\(.*?\)|for\W*?\(.*?\)|try|catch\W*?\(.*?\)|switch\W*?\(.*?\))");
            if (regex_search(states.front().buffer, controlConstructionCheck)) {
                stats.logicalLines++;
                states.front().buffer = "";
            } else if (c == '{')
                states.push_front(state{FUNCTION});
            else if (c == ';') {
                bool seemsEmpty = true;
                for (char &q : states.front().buffer) {
                    if ((q >= 'a' && q <= 'z') || (q >= 'A' && q <= 'Z') || (q >= '0' && q <= '9')) {
                        seemsEmpty = false;
                        break;
                    }
                }
                if (!seemsEmpty)
                    stats.logicalLines++;
                states.front().buffer = "";
            } else if (c == '}')
                states.pop_front();
        } else if (states.front().block == STRUCT) {
            if (c == ';') {
                bool seemsEmpty = true;
                for (char &q : states.front().buffer) {
                    if ((q >= 'a' && q <= 'z') || (q >= 'A' && q <= 'Z') || (q >= '0' && q <= '9')) {
                        seemsEmpty = false;
                        break;
                    }
                }
                if (!seemsEmpty)
                    stats.logicalLines++;
                states.front().buffer = "";
            }
            if (c == '{') {
                regex classCheckRegex(R"(class|struct .*?{)");
                regex functionCheckRegex(R"(.*? .*?\(.*?\).*?{)");
                if (regex_search(states.front().buffer, classCheckRegex))
                    states.push_front(state{STRUCT});
                else if (regex_search(states.front().buffer, functionCheckRegex))
                    states.push_front(state{FUNCTION});
                else
                    states.push_front(state{BLOCK});
                stats.logicalLines++;
            }
            if (c == '}')
                states.pop_front();
        }
    }
    if (file[file.size() - 1] != '\n')
        stats.physicalLines++;
    return stats;
}
