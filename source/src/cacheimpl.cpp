#include <utility>

#include "cacheimpl.h"
#include "logger.h"
#include "fiber_pool.h"
#include <memory>
#include <boost/filesystem/fstream.hpp>
#include <iostream>
#include <boost/filesystem.hpp>
#include <boost/fiber/fiber.hpp>
#include <boost/atomic.hpp>
#include <stdexcept>

using namespace std;
using namespace boost::fibers;
using namespace boost::posix_time;
using namespace boost::filesystem;

const ptime epoch(boost::gregorian::date(1970, 1, 1));

CacheImpl::CacheImpl(const path root, const path cacheDir) : Cache(), rootDir(root), taskQueue(16), taskArgsQueue() {
    auto l = Logger::get_logger("Cache");
    this->root = make_shared<CacheTreeDirectoryItem>(root.filename().string());

    BOOST_LOG_SEV(l, info) << "Scanning files...";

    if (!is_directory(cacheDir)) {
        if (exists(cacheDir))
            throw "Can't create cache directory: cache directory is a file";
        create_directory(cacheDir);
    }

    bool filesExist = true;

    BOOST_LOG_SEV(l, info) << "Scanning hashes.list existence...";
    const path hashesFile = cacheDir / "hashes.list";
    if (!is_regular_file(hashesFile)) {
        if (exists(hashesFile))
            throw "Can't create hashes.list cache file: caches.list is a directory";
        boost::filesystem::ofstream stream(hashesFile, std::ios::binary);
        int count = 0;
        stream.write((char *) &count, sizeof(int));
        stream.close();
        filesExist = false;
    }

    BOOST_LOG_SEV(l, info) << "Scanning files.stats existence...";
    const path statsFile = cacheDir / "files.stats";
    if (!is_regular_file(statsFile)) {
        if (exists(statsFile))
            throw "Can't create files.stats cache file: files.stats is a directory";
        boost::filesystem::ofstream stream(statsFile);
        stream.close();
        filesExist = false;
    }

    BOOST_LOG_SEV(l, info) << "Initiating fragment managers...";
    hashesFragmentationManager = make_unique<FragmentationManager>(
            cacheDir / "hashes.fragmentation");
    statsFragmentationManager = make_unique<FragmentationManager>(
            cacheDir / "stats.fragmentation");
    int realEntryCount = filesExist ? read_entries(hashesFile, statsFile) : 0;
    writer = new fiber(boost::fibers::launch::dispatch, [this, hashesFile, statsFile, realEntryCount]() {
        FiberPool::acknowledge_new_fiber();
        this->run_writer(hashesFile, statsFile, realEntryCount);
        FiberPool::acknowledge_fiber_dead();
    });
}

int CacheImpl::read_entries(const path hashesFile, const path statsFile) {
    auto l = Logger::get_logger("Cache");
    int realEntryCount = 0;
    BOOST_LOG_SEV(l, info) << "Reading files...";
    boost::filesystem::ifstream hashesStream(hashesFile, std::ios::binary);
    boost::filesystem::ifstream statsStream(statsFile, std::ios::binary);
    int entryCount = 0;
    hashesStream.read((char *) &entryCount, sizeof(int));
    BOOST_LOG_SEV(l, info) << "Found " << entryCount << " entries";
    for (int i = 0; i < entryCount; ++i) {
        CacheFileEntry entry;
        hashesStream >> entry;
        path file = this->rootDir / path(entry.name);
        int entryPos = (int) hashesStream.tellg() - entry.size();
        if (!is_regular_file(file)) {//Delete entry
            hashesFragmentationManager->add_empty_fragment(entryPos, entry.size());
            statsFragmentationManager->add_empty_fragment(entry.cacheFilePos, FileStatistics::size);
            BOOST_LOG_SEV(l, info) << "File " + file.string() + " was deleted from cache";
        } else {
            FileStatistics buf;
            statsStream.seekg(entry.cacheFilePos);
            statsStream >> buf;
            shared_ptr<FileStatistics> stats = make_shared<FileStatistics>(&buf);
            auto name = file.filename().string();
            shared_ptr<CacheTreeFileItem> item = make_shared<CacheTreeFileItem>(file, entry.hash,
                                                                                entry.changeTime,
                                                                                entry.cacheFilePos, entryPos,
                                                                                stats);
            item->empty = false;
            add_file_item(file, item);
            realEntryCount++;
        }
    }
    BOOST_LOG_SEV(l, info) << "Red " << entryCount << " entries";
    return realEntryCount;
}

CacheImpl::~CacheImpl() {
    taskQueue.push(END);
    writer->join();
    delete writer;
    hashesFragmentationManager = nullptr;
    statsFragmentationManager = nullptr;
    root = nullptr;
}

bool CacheImpl::contains(const path path) {
    auto item = get_file_item(path);
    return item && !item->empty;
}

string CacheImpl::get_checksum(const path path) {
    auto item = get_file_item(path);
    return (item && !item->empty) ? item->checksum : "";
}

ptime CacheImpl::get_change_time(const path path) {
    auto item = get_file_item(path);
    return (item && !item->empty) ? item->changeTime : ptime();
}

FileStatistics CacheImpl::get_file_stats(const path path) {
    auto item = get_file_item(path);
    if (!item || item->empty)
        throw std::runtime_error("File not found");
    return *item->stats;
}

void CacheImpl::prepare(const path path) {
    if (!!get_file_item(path))
        return;
    string defName = "";
    auto new_item = make_shared<CacheTreeFileItem>(path, defName, ptime(), -1, -1,
                                                   make_shared<FileStatistics>());
    add_file_item(path, new_item);
}

void CacheImpl::put_file_stats(const path path, FileStatistics stats) {
    auto item = get_file_item(path);
    if (!item)
        throw std::runtime_error("Path " + path.string() + " was not prepared");
    else {
        item->stats = make_shared<FileStatistics>(stats);
        if (item->empty)
            submit_task(ADD, item);
        else
            submit_task(UPDATE, item);
        item->empty = false;
    }
}

void CacheImpl::put_checksum(const path path, string checksum) {
    auto item = get_file_item(path);
    if (!item)
        throw std::runtime_error("Path " + path.string() + " was not prepared");
    else {
        item->checksum = checksum;
        if (item->empty)
            submit_task(ADD, item);
        else
            submit_task(UPDATE, item);
        item->empty = false;
    }
}

void CacheImpl::put_change_time(const path path, ptime time) {
    auto item = get_file_item(path);
    if (!item)
        throw std::runtime_error("Path " + path.string() + " was not prepared");
    else {
        item->changeTime = time;
        if (item->empty)
            submit_task(ADD, item);
        else
            submit_task(UPDATE, item);
        item->empty = false;
    }
}

void CacheImpl::submit_task(const TaskType type, shared_ptr<CacheTreeFileItem> new_item) {
    taskArgsQueue.push(new_item);
    taskQueue.push(type);
}

void CacheImpl::add_file_item(path path, shared_ptr<CacheTreeFileItem> item) {
    auto dirItem = add_directory_item(path.parent_path());
    string name = path.filename().string();
    auto last = dirItem->files.insert({name, item});
    if (!last.second)
        throw std::runtime_error("File exists");
}

shared_ptr<CacheTreeDirectoryItem> CacheImpl::add_directory_item(path dir) {
    if (dir == rootDir)
        return root;

    string name = dir.filename().string();
    shared_ptr<CacheTreeDirectoryItem> parentDir = add_directory_item(dir.parent_path());
    if (parentDir->directories.count(name))
        return parentDir->directories[name];

    shared_ptr<CacheTreeDirectoryItem> directory = make_shared<CacheTreeDirectoryItem>(name);
    parentDir->directories.insert({name, directory});
    return directory;
}

shared_ptr<CacheTreeDirectoryItem> CacheImpl::get_directory_item(path dir) {
    if (dir == rootDir)
        return root;

    string name = dir.filename().string();
    shared_ptr<CacheTreeDirectoryItem> parentDir = get_directory_item(dir.parent_path());
    if (!parentDir)
        return shared_ptr<CacheTreeDirectoryItem>();
    if (parentDir->directories.count(name))
        return parentDir->directories[name];
    else
        return shared_ptr<CacheTreeDirectoryItem>();
}

shared_ptr<CacheTreeFileItem> CacheImpl::get_file_item(path file) {
    auto dirItem = get_directory_item(file.parent_path());
    return (dirItem && dirItem->files.count(file.filename().string())) ? dirItem->files[file.filename().string()]
                                                                       : shared_ptr<CacheTreeFileItem>();
}

void CacheImpl::run_writer(path hashesFile, path statsFile, int entryCount) {
    auto l = Logger::get_logger("CacheWriterFiber");
    BOOST_LOG_SEV(l, info) << "Starting...";

    unsigned long hashesFileSize = file_size(hashesFile);
    unsigned long statsFileSize = file_size(statsFile);

    boost::filesystem::fstream hashesStream(hashesFile, std::ios::binary | std::ios_base::out | std::ios_base::in);
    boost::filesystem::fstream statsStream(statsFile, std::ios::binary | std::ios_base::out | std::ios_base::in);
    BOOST_LOG_SEV(l, info) << "Running...";
    int count = entryCount;
    while (true) {
        TaskType t;
        taskQueue.pop(std::ref(t));
        if (t == END)
            break;
        if (t == ADD) {
            auto item = taskArgsQueue.pull();
            int statsPos = statsFragmentationManager->acquire_next_empty_fragment(FileStatistics::size);
            if (statsPos < 0) {
                statsPos = statsFileSize;
                statsFileSize += FileStatistics::size;
            }
            statsStream.seekp(statsPos);

            CacheFileEntry entry{relative(item->path, rootDir).string(), item->checksum, (unsigned int) statsPos,
                                 item->changeTime};

            int pos = hashesFragmentationManager->acquire_next_empty_fragment(entry.size());
            if (pos < 0) {
                pos = hashesFileSize;
                hashesFileSize += entry.size();
            }
            hashesStream.seekp(pos);

            hashesStream << entry;
            statsStream << *item->stats;
            item->filePos = static_cast<unsigned int>(statsPos);
            item->checksumFilePos = static_cast<unsigned int>(pos);

            count++;
            hashesStream.seekp(0);
            hashesStream.write((char *) &count, sizeof(count));
        } else if (t == UPDATE) {
            auto item = taskArgsQueue.pull();
            CacheFileEntry newEntry{relative(item->path, rootDir).string(), item->checksum, item->filePos,
                                    item->changeTime};
            hashesStream.seekp(item->checksumFilePos);
            hashesStream << newEntry;
            statsStream.seekp(item->filePos);
            statsStream << *item->stats;
        }
    }
    BOOST_LOG_SEV(l, info) << "Stopping...";
    hashesStream.close();
    statsStream.close();
    BOOST_LOG_SEV(l, info) << "Stopped";
}

CacheTreeFileItem::CacheTreeFileItem(class boost::filesystem::path path, string checksum, ptime changeTime,
                                     unsigned int filePos,
                                     unsigned int checksumFilePos, shared_ptr<FileStatistics> stats)
        : path(std::move(path)),
          checksum(checksum),
          changeTime(changeTime),
          filePos(filePos),
          checksumFilePos(checksumFilePos),
          stats(std::move(stats)),
          empty(true) {
}

CacheTreeFileItem::CacheTreeFileItem(CacheTreeFileItem *item) :
        path(item->path),
        checksum(item->checksum),
        changeTime(item->changeTime),
        filePos(item->filePos),
        checksumFilePos(item->checksumFilePos),
        stats(item->stats),
        empty(item->empty) {
}

CacheTreeDirectoryItem::CacheTreeDirectoryItem(const string &name) : name(name), files(), directories() {}

int CacheFileEntry::size() {
    return sizeof(int) + name.size() + sizeof(int) + hash.size() + sizeof(int) + sizeof(long);
}

istream &operator>>(istream &stream, CacheFileEntry &entry) {
    unsigned int nameLen = 0;
    stream.read((char *) &nameLen, sizeof(nameLen));
    char *nameArr = new char[nameLen];
    stream.read(nameArr, sizeof(char) * nameLen);
    entry.name = string(nameArr, nameLen);
    delete[] nameArr;

    unsigned int hashLen = 0;
    stream.read((char *) &hashLen, sizeof(hashLen));
    char *hashArr = new char[hashLen];
    stream.read(hashArr, sizeof(char) * hashLen);
    entry.hash = string(hashArr, hashLen);
    delete[] hashArr;

    unsigned int cacheFilePos = 0;
    stream.read((char *) &cacheFilePos, sizeof(cacheFilePos));
    entry.cacheFilePos = cacheFilePos;

    unsigned long changeTime = 0;
    stream.read((char *) &changeTime, sizeof(changeTime));
    ptime time = epoch + milliseconds(changeTime);
    entry.changeTime = time;
    return stream;
}

ostream &operator<<(ostream &stream, const CacheFileEntry &entry) {
    unsigned int nameSize = entry.name.size();
    stream.write((char *) &nameSize, sizeof(int));
    stream.write(entry.name.c_str(), nameSize);
    unsigned int hashSize = entry.hash.size();
    stream.write((char *) &hashSize, sizeof(int));
    stream.write(entry.hash.c_str(), hashSize);
    unsigned int cacheFilePos = entry.cacheFilePos;
    stream.write((char *) &cacheFilePos, sizeof(cacheFilePos));
    unsigned long time = (entry.changeTime - epoch).total_milliseconds();
    stream.write((char *) &time, sizeof(time));

    return stream;
}