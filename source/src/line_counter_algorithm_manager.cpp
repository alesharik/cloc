#include "line_counter_algorithm_manager.h"
#include "cpp_line_counter_algorithm.h"
#include <stdexcept>

std::map<std::string, LineCounterAlgorithm *> LineCounterAlgorithmManager::algorithms;

void LineCounterAlgorithmManager::init() {
    auto cpp = new CppLineCounterAlgorithm();
    LineCounterAlgorithmManager::algorithms[".h"] = cpp;
    LineCounterAlgorithmManager::algorithms[".c"] = cpp;
    LineCounterAlgorithmManager::algorithms[".cxx"] = cpp;
    LineCounterAlgorithmManager::algorithms[".cpp"] = cpp;
    LineCounterAlgorithmManager::algorithms[".hpp"] = cpp;
}

bool LineCounterAlgorithmManager::support_file_extension(const std::string extension) {
    return algorithms.count(extension) > 0;
}

LineCounterAlgorithm *LineCounterAlgorithmManager::get_for_file_extension(const std::string extension) {
    if (!support_file_extension(extension))
        throw std::runtime_error("File extension " + extension + " is not supported!");
    return algorithms[extension];
}
