#ifndef CLOC_ASYNC_FILE_H
#define CLOC_ASYNC_FILE_H

#include <memory>
#include <boost/filesystem/path.hpp>
#include <boost/fiber/unbuffered_channel.hpp>
#include <boost/fiber/fiber.hpp>
#include <boost/atomic.hpp>

typedef struct {
    unsigned long length;
    ///MUST BE FREED AFTER READ
    char *data;
} file_part;

class AsyncFile {
    static const int bufLength = 4096;

    boost::filesystem::path path;
    boost::fibers::fiber *reader;
    boost::atomic_bool working;
    std::shared_ptr<boost::fibers::unbuffered_channel<file_part>> channel;
public:
    AsyncFile(boost::filesystem::path path);

    virtual ~AsyncFile();

    void open(std::shared_ptr<boost::fibers::unbuffered_channel<file_part>> channel);

    void close();
};

#endif //CLOC_ASYNC_FILE_H
