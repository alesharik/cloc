#include <boost/program_options.hpp>
#include <iostream>
#include <boost/fiber/all.hpp>
#include <boost/filesystem.hpp>
#include <boost/locale/info.hpp>
#include <boost/locale/generator.hpp>
#include <boost/bind.hpp>
#include <boost/fiber/unbuffered_channel.hpp>
#include "hashing.h"
#include "fiber_pool.cpp"
#include "logger.h"
#include "cache.h"
#include "cacheimpl.h"
#include "file_preprocessor.h"
#include "date_file_preprocessor.h"
#include "checksum_file_preprocessor.h"
#include "async_file.h"
#include "line_counter_algorithm.h"

#include <thread>
#include <chrono>
#include "charset_converter.h"
#include "charset_decoder.h"
#include "line_counter_algorithm_manager.h"

using namespace boost::program_options;
using namespace boost::filesystem;
using namespace boost::fibers;
using namespace std;

string get_charset(const path path) {
    const int SCAN_LENGTH = 4096;
    AsyncFile scanFile(path);
    vector<file_part> scanParts;
    unsigned long scanLength = 0;
    auto channel = make_shared<boost::fibers::unbuffered_channel<file_part>>();
    scanFile.open(channel);
    file_part part;
    while (boost::fibers::channel_op_status::success == channel->pop(std::ref(part))) {
        scanParts.push_back(part);
        scanLength += part.length;
        if (scanLength > SCAN_LENGTH)
            scanFile.close();
    }
    char *scanData = new char[scanLength];
    unsigned long scanDataWritePos = 0;
    for (auto &part : scanParts) {
        memcpy(scanData + scanDataWritePos * sizeof(char), part.data, part.length);
        scanDataWritePos += part.length;
        delete[] part.data;
    }

    return CharsetDecoder::decode(scanData, scanLength, boost::filesystem::file_size(path) == scanLength);
}

FileStatistics
fiber_run(const path path, vector<shared_ptr<FilePreprocessorFactory>> &preprocessors, shared_ptr<Cache> cache) {
    auto l = Logger::get_logger(path.string());
    string ext = path.extension().string();
    if (!LineCounterAlgorithmManager::support_file_extension(ext)) {
        BOOST_LOG_SEV(l, severity_level::warn) << "Extension " << ext << " is not supported!";
        return FileStatistics();
    }

    vector<unique_ptr<FilePreprocessor>> preproc;
    for (auto &proc : preprocessors)
        preproc.push_back(std::move(proc->create(path)));
    BOOST_LOG_SEV(l, severity_level::info) << "Preprocessing...";
    for (auto &proc : preproc) {
        proc->preprocess_file();
        if (proc->is_file_already_processed()) {
            BOOST_LOG_SEV(l, severity_level::info) << "Returning stats from cache...";
            return cache->get_file_stats(path);
        }
    }

    BOOST_LOG_SEV(l, severity_level::info) << "Decoding charset...";
    string charset = get_charset(path);
    BOOST_LOG_SEV(l, severity_level::info) << "Charset: " << charset;
    if (charset.empty()) {
        BOOST_LOG_SEV(l, severity_level::error) << "Can't detect encoding of a file " << path.string()
                                                << "! Ignoring...";
        return FileStatistics();
    }

    BOOST_LOG_SEV(l, severity_level::info) << "Reading...";
    size_t length = boost::filesystem::file_size(path);
    char *in = new char[length];
    unsigned long pos = 0;
    AsyncFile file(path);
    auto ch = make_shared<boost::fibers::unbuffered_channel<file_part>>();
    file.open(ch);
    file_part buf;
    while (boost::fibers::channel_op_status::success == ch->pop(std::ref(buf))) {
        memcpy(in + pos, buf.data, buf.length);
        delete[] buf.data;
    }
    BOOST_LOG_SEV(l, severity_level::info) << "Reading done: " << length << " bytes";

    string content = CharsetConverter::to_utf8(in, length, charset);
    BOOST_LOG_SEV(l, severity_level::info) << "Parsing file, extension: " << ext;

    FileStatistics ret = LineCounterAlgorithmManager::get_for_file_extension(ext)->count(content);

    BOOST_LOG_SEV(l, severity_level::info) << "Postprocessing..." << ext;
    for (auto &proc : preproc) {
        proc->finish_processing();
        proc = nullptr;
    }
    BOOST_LOG_SEV(l, severity_level::info) << "Writing result to the cache...";
    cache->put_file_stats(path, ret);
    BOOST_LOG_SEV(l, severity_level::info) << "Done";
    return ret;
}

int main(int argc, char **argv) {
    Logger::init();
    CharsetDecoder::init("ru");
    LineCounterAlgorithmManager::init();

    unsigned int concurrency = std::thread::hardware_concurrency();
    if (concurrency == 1)
        use_scheduling_algorithm<algo::round_robin>();
    else
        use_scheduling_algorithm<algo::work_stealing>(concurrency);

    options_description desc("Options");
    desc.add_options()
            ("help,h", "show help")
            ("verbose,v", "be verbose")
            ("root,r", value<vector<string>>(), "root directory")
            ("hashing,h", value<bool>()->default_value(true), "is hashing enabled")
            ("changedate,d", value<bool>()->default_value(true), "is change date checking enabled")
            ("hash,ha", value<HashingAlgorithm>()->default_value(HashingAlgorithm::CRC32),
             "set hashing algorithm (SUPPORTED_ALGORITHMS_STRING)")
            ("cache,c", value<bool>()->default_value(true), "enable cache")
            ("cache-directory", value<vector<string>>(), "cache directory relative to root")
            ("use-gitignore", value<bool>()->default_value(true),
             "use .gitignore file to ignore directories. NOT IMPLEMENTED")
            ("exclude", value<vector<string>>(), "exclude directories from scanning. NOT IMPLEMENTED");

    variables_map map;
    try {
        store(command_line_parser(argc, argv)
                      .options(desc)
                      .run(), map);
        if (map.count("help")) {
            cout << "This program counts lines of code in given folder";
            //TODO
            return 0;
        }
        notify(map);
    } catch (required_option &e) {
        //TODO
    } catch (exception &e) {
        //TODO
    }

    if (map.count("verbose"))
        Logger::set_severity_level(severity_level::debug);

    FiberPool::start();

    path root(map.count("root") > 0 ? map["root"].as<vector<string>>()[0] : "./");
    path cacheDir(map.count("cache-directory") > 0 ? map["cache-directory"].as<vector<string>>()[0] : ".cloc");

    shared_ptr<Cache> cache =
            map.count("cache") && map["cache"].as<bool>() ? (shared_ptr<Cache>) make_shared<CacheImpl>(root, cacheDir)
                                                          : make_shared<CacheStub>();

    vector<shared_ptr<FilePreprocessorFactory>> preprocessors;
    if (map.count("cache") && map["cache"].as<bool>()) {
        if (map.count("changedate") && map["changedate"].as<bool>())
            preprocessors.push_back(make_shared<DateFilePreprocessorFactory>(cache));

        shared_ptr<Hasher> hasher = map.count("hashing") && map["hashing"].as<bool>() ? make_shared<Hasher>(
                map["hash"].as<HashingAlgorithm>())
                                                                                      : shared_ptr<Hasher>();
        if (hasher)
            preprocessors.push_back(make_shared<ChecksumFileProcessorFactory>(cache, hasher));
    }

    recursive_directory_iterator dir(root), end;
    unbuffered_channel<FileStatistics> channel;
    int count = 0;
    while (dir != end) {
        //dir.no_push() - do not scan directory
        if (is_regular_file(dir->path())) {
            const path path = dir->path();
            cache->prepare(path);
            fiber([path, &preprocessors, &cache, &channel]() {
                FiberPool::acknowledge_new_fiber();
                FileStatistics stats = fiber_run(path, preprocessors, cache);
                channel.push(stats);
//                cache.reset();
                FiberPool::acknowledge_fiber_dead();
            }).detach();
            count++;
        }
        ++dir;
    }
    const int cnt = count;
    FileStatistics global;
    FileStatistics i;
    while (boost::fibers::channel_op_status::success == channel.pop(std::ref(i))) {
        global += i;
        cout << "Processed file " << cnt - count + 1 << " of " << cnt << endl;
        --count;
        if (count <= 0)
            channel.close();
    }

    cout << "Physical lines: " << global.physicalLines << endl
         << "Logical lines: " << global.logicalLines << endl
         << "Comment lines: " << global.commentLines << endl
         << "Precompiler expressions: " << global.precompilerExpressions << endl
         << "Blank lines: " << global.blankLines << endl;

    cache.reset();

    FiberPool::shutdown();

    return 0;
}

