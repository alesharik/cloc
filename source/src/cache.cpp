#include "cache.h"

bool CacheStub::contains(path path) {
    return false;
}

string CacheStub::get_checksum(path path) {
    return "";
}

FileStatistics CacheStub::get_file_stats(path path) {
    return FileStatistics();
}

ptime CacheStub::get_change_time(path path) {
    return ptime();
}

void CacheStub::put_checksum(path path, string checksum) {

}

void CacheStub::put_change_time(path path, ptime time) {

}

void CacheStub::put_file_stats(path path, FileStatistics stats) {

}

void CacheStub::prepare(path path) {

}

CacheStub::~CacheStub() = default;

Cache::~Cache() {

}
