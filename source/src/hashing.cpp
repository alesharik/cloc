#include "hashing.h"
#include <iomanip>
#include <sstream>
#include <stdexcept>

std::istream &operator>>(std::istream &in, HashingAlgorithm &algorithm) {
    std::string token;
    in >> token;
#ifdef OpenSSL_FOUND
    if (token == "md5")
        algorithm = HashingAlgorithm::MD5;
    else
#endif
    if (token == "crc32")
        algorithm = HashingAlgorithm::CRC32;
    else
        in.setstate(std::ios_base::failbit);

    return in;
}

#ifdef OpenSSL_FOUND

#include <openssl/evp.h>

MD5Hash::MD5Hash() {
    ctx = EVP_MD_CTX_create();
}

MD5Hash::~MD5Hash() {
    EVP_MD_CTX_destroy(ctx);
}

void MD5Hash::init() {
    if (EVP_DigestInit_ex(ctx, EVP_md5(), nullptr) != 1)
        throw std::runtime_error("Can't initialize message digest");
}

void MD5Hash::update(const char *data, unsigned int length) {
    if (EVP_DigestUpdate(ctx, data, length) != 1)
        throw std::runtime_error("Can't update message digest");
}

std::string MD5Hash::final() {
    unsigned char container[EVP_MAX_MD_SIZE];
    unsigned int len;
    if (EVP_DigestFinal_ex(ctx, container, &len) != 1)
        throw std::runtime_error("Can't finalize message digest");
    ctx = nullptr;

    std::stringstream ss;
    ss << std::hex;
    for (int i = 0; i < len; ++i) {
        ss << container[i];
    }

    return ss.str();
}

#endif

#ifdef ZLIB_FOUND

CRC32Hash::CRC32Hash() {
    result = crc32(0L, Z_NULL, 0);
}

void CRC32Hash::init() {
    result = crc32(0L, Z_NULL, 0);
}

void CRC32Hash::update(const char *data, unsigned int length) {
    result = crc32(result, (const Bytef *) data, length);
}

std::string CRC32Hash::final() {
    std::stringstream ss;
    ss << std::hex << result;
    return ss.str();
}

#else

void CRC32Hash::init() {
    crc.reset();
}

void CRC32Hash::update(const char *data, unsigned int length) {
    crc.process_bytes(data, length);
}

std::string CRC32Hash::final() {
    std::stringstream ss;
    ss << std::hex << crc.checksum();
    return ss.str();
}

#endif

Hasher::Hasher(HashingAlgorithm algorithm) : algorithm(algorithm) {}

Hash *Hasher::newHash() {
    if (algorithm == HashingAlgorithm::CRC32)
        return new CRC32Hash();
#ifdef OpenSSL_FOUND
    if (algorithm == HashingAlgorithm::MD5)
        return new MD5Hash();
#endif
    throw std::runtime_error("Hashing algorithm with id " + std::to_string(algorithm) + " not found");
}

void HashDeleter::operator()(Hash *h) const {
    delete h;
}
