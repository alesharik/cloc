#include "statistics.h"

using namespace std;

FileStatistics::FileStatistics(FileStatistics *pStatistics) : physicalLines(pStatistics->physicalLines),
                                                              logicalLines(pStatistics->logicalLines),
                                                              blankLines(pStatistics->blankLines),
                                                              commentLines(pStatistics->commentLines),
                                                              precompilerExpressions(
                                                                      pStatistics->precompilerExpressions) {
}

FileStatistics::FileStatistics() : physicalLines(0), logicalLines(0), blankLines(0), commentLines(0),
                                   precompilerExpressions(0) {
}

FileStatistics::FileStatistics(int physicalLines, int logicalLines, int commentLines, int blankLines) : physicalLines(
        physicalLines), logicalLines(logicalLines), commentLines(commentLines), blankLines(blankLines),
                                                                                                        precompilerExpressions(
                                                                                                                0) {}

FileStatistics::FileStatistics(int physicalLines, int logicalLines, int commentLines, int blankLines,
                               int precompilerExpressions) : physicalLines(
        physicalLines), logicalLines(logicalLines), commentLines(commentLines), blankLines(blankLines),
                                                             precompilerExpressions(precompilerExpressions) {}

bool FileStatistics::operator==(const FileStatistics &other) const {
    return physicalLines == other.physicalLines && logicalLines == other.logicalLines &&
           commentLines == other.commentLines && blankLines == other.blankLines &&
           precompilerExpressions == other.precompilerExpressions;
}

void FileStatistics::operator+=(const FileStatistics &other) {
    physicalLines += other.physicalLines;
    logicalLines += other.logicalLines;
    blankLines += other.blankLines;
    commentLines += other.commentLines;
    precompilerExpressions += other.precompilerExpressions;
}

istream &operator>>(istream &stream, FileStatistics &stats) {
    stream.read((char *) &(stats.physicalLines), sizeof(FileStatistics::physicalLines));
    stream.read((char *) &(stats.logicalLines), sizeof(FileStatistics::logicalLines));
    stream.read((char *) &(stats.blankLines), sizeof(FileStatistics::blankLines));
    stream.read((char *) &(stats.commentLines), sizeof(FileStatistics::commentLines));
    stream.read((char *) &(stats.precompilerExpressions), sizeof(FileStatistics::precompilerExpressions));
    return stream;
}

ostream &operator<<(ostream &stream, const FileStatistics &stats) {
    stream.write((char *) &(stats.physicalLines), sizeof(FileStatistics::physicalLines));
    stream.write((char *) &(stats.logicalLines), sizeof(FileStatistics::logicalLines));
    stream.write((char *) &(stats.blankLines), sizeof(FileStatistics::blankLines));
    stream.write((char *) &(stats.commentLines), sizeof(FileStatistics::commentLines));
    stream.write((char *) &(stats.precompilerExpressions), sizeof(FileStatistics::precompilerExpressions));
    return stream;
}