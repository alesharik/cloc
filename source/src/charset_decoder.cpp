#include "charset_decoder.h"
#include <math.h>
#include <stdexcept>

std::string CharsetDecoder::language;

std::string CharsetDecoder::decode(const char *data, size_t length, bool endOfFile) {
    //Constants from ENCA executable( https://github.com/nijel/enca/blob/master/src/enca.c )
    thread_local EncaAnalyser analyser = enca_analyser_alloc(language.c_str());
    enca_set_threshold(analyser, 1.38);
    enca_set_multibyte(analyser, 1);
    enca_set_ambiguity(analyser, 1);
    enca_set_garbage_test(analyser, 1);

    const double mu = 0.005;  /* derivation in 0 */
    const double m = 15.0;  /* value in infinity */
    auto sgnf = static_cast<size_t>(std::ceil((double) length / (length / m + 1.0 / mu)));
    enca_set_significant(analyser, sgnf);
    enca_set_termination_strictness(analyser, endOfFile ? 1 : 0);
    enca_set_filtering(analyser, sgnf > 2);

    EncaEncoding encoding = enca_analyse_const(analyser, (const unsigned char *) data, length);
    if (!enca_charset_is_known(encoding.charset))
        return "";
    return std::string(enca_charset_name(encoding.charset, EncaNameStyle::ENCA_NAME_STYLE_ICONV));
}

void CharsetDecoder::init(const std::string &lang) {
    EncaAnalyser analyser = enca_analyser_alloc(lang.c_str());
    if (!analyser)
        throw std::runtime_error("Language " + lang + " is not supported!");
    enca_analyser_free(analyser);
    language = lang;
}
