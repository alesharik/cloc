#ifndef CLOC_FILE_PREPROCESSOR_H
#define CLOC_FILE_PREPROCESSOR_H

#include <boost/filesystem/path.hpp>
#include <memory>

class FilePreprocessor {
public:
    virtual ~FilePreprocessor() = default;

    virtual void preprocess_file() = 0;

    virtual bool is_file_already_processed() = 0;

    virtual void finish_processing() = 0;
};

class FilePreprocessorFactory {
public:
    virtual std::unique_ptr<FilePreprocessor> create(const boost::filesystem::path path) = 0;
};

#endif //CLOC_FILE_PREPROCESSOR_H
