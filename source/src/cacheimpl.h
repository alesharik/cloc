#ifndef CLOC_CACHEIMPL_H
#define CLOC_CACHEIMPL_H

#include <map>
#include <memory>
#include <boost/fiber/fiber.hpp>
#include <boost/atomic.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/lockfree/queue.hpp>
#include <boost/fiber/buffered_channel.hpp>
#include <boost/thread/sync_queue.hpp>
#include <boost/date_time.hpp>
#include "fragmentation_manager.h"
#include "cache.h"

/*
 * hashes file:
 * count
 * name hash cacheFilePos dateChanged
 * ...
 */

namespace {
    struct CacheTreeFileItem {
        boost::filesystem::path path;
        ///Must have constant size for the project
        string checksum;
        ptime changeTime;
        ///Position in cache file in bytes
        unsigned int filePos;
        ///Position in checksum file in bytes
        unsigned int checksumFilePos;
        shared_ptr<FileStatistics> stats;
        bool empty;

        CacheTreeFileItem(class path path, string checksum, ptime changeTime, unsigned int filePos,
                          unsigned int checksumFilePos,
                          shared_ptr<FileStatistics> stats);

        explicit CacheTreeFileItem(CacheTreeFileItem *item);
    };

    struct CacheTreeDirectoryItem {//FIXME potential memory/performance problem(performance may be slow when there are a lot of folders with not too many files)
        string name;
        map<string, shared_ptr<CacheTreeFileItem>> files;
        map<string, shared_ptr<CacheTreeDirectoryItem>> directories;

        explicit CacheTreeDirectoryItem(const string &name);
    };

    struct CacheFileEntry {
        string name;
        string hash;
        unsigned int cacheFilePos;
        ptime changeTime;

        int size();
    };


    enum TaskType {
        ADD,
        UPDATE,
        END
    };
}

istream &operator>>(istream &stream, CacheFileEntry &entry);

ostream &operator<<(ostream &stream, const CacheFileEntry &entry);

class CacheImpl : public Cache {
    shared_ptr<CacheTreeDirectoryItem> root;
    path rootDir;
    boost::fibers::buffered_channel<TaskType> taskQueue;
    boost::sync_queue<shared_ptr<CacheTreeFileItem>> taskArgsQueue;
    fiber *writer;
    unique_ptr<FragmentationManager> hashesFragmentationManager;
    unique_ptr<FragmentationManager> statsFragmentationManager;
public:
    explicit CacheImpl(path root, path cacheDir);

    ~CacheImpl() override;

    void prepare(path path) override;

    bool contains(path path) override;

    string get_checksum(path path) override;

    ptime get_change_time(path path) override;

    void put_checksum(path path, string checksum) override;

    void put_change_time(path path, ptime time) override;

    void put_file_stats(path path, FileStatistics stats) override;

    FileStatistics get_file_stats(path path) override;

private:
    void add_file_item(path parent, shared_ptr<CacheTreeFileItem> item);

    shared_ptr<CacheTreeFileItem> get_file_item(path file);

    shared_ptr<CacheTreeDirectoryItem> get_directory_item(path dir);

    shared_ptr<CacheTreeDirectoryItem> add_directory_item(path dir);

    void submit_task(TaskType type, shared_ptr<CacheTreeFileItem> new_item);

    void run_writer(path hashesFile, path statsFile, int entryCount);

    int read_entries(path hashesFile, path statsFile);
};

#endif //CLOC_CACHEIMPL_H
