#include "fragmentation_manager.h"
#include "logger.h"
#include "fiber_pool.h"
#include <boost/fiber/fiber.hpp>
#include <vector>
#include <chrono>
#include <boost/filesystem/fstream.hpp>
#include <boost/filesystem.hpp>

using namespace boost::fibers;
using namespace boost::filesystem;

std::istream &operator>>(std::istream &stream, fragment &frag) {
    stream.read((char *) &(frag.pos), sizeof(fragment::pos));
    stream.read((char *) &(frag.size), sizeof(fragment::size));
    return stream;
}

std::ostream &operator<<(std::ostream &stream, const fragment &frag) {
    stream.write((char *) &(frag.pos), sizeof(fragment::pos));
    stream.write((char *) &(frag.size), sizeof(fragment::size));
    return stream;
}

FragmentationManager::FragmentationManager(const path &file) : write_trigger(),
                                                               write_trigger_mutex(),
                                                               access_count(0),
                                                               snapshot_running(false),
                                                               running(true),
                                                               fragments(16) {
    auto l = Logger::get_logger("FragmentManager: " + file.filename().string());
    BOOST_LOG_SEV(l, info) << "Checking file...";
    if (!is_regular_file(file)) {
        if (exists(file))
            throw "File " + file.string() + " must be a file";
    } else {
        BOOST_LOG_SEV(l, info) << "Reading file...";
        ifstream in(file, std::ios::binary);
        unsigned long count = 0;
        in.read((char *) &count, sizeof(count));
        for (unsigned long i = 0; i < count; ++i) {
            fragment frag{0, 0};
            in >> frag;
            fragments.unsynchronized_push(frag);
        }
        in.close();
        BOOST_LOG_SEV(l, info) << "File has been red. Found " + std::to_string(count) + " entries";
    }
    this->save_fiber = new fiber(boost::fibers::launch::dispatch, [this, file]() { this->fiber_run(file); });
    BOOST_LOG_SEV(l, info) << "Fiber has been spawned";
}

FragmentationManager::~FragmentationManager() {
    running = false;
    write_trigger.notify_all();
    this->save_fiber->join();
    delete this->save_fiber;
}

void FragmentationManager::add_empty_fragment(int pos, int length) {
    if (!fragments.push(fragment{pos, length}))
        throw "Can't push an element in stack";
    write_trigger.notify_one();
}

std::shared_ptr<std::vector<fragment>> FragmentationManager::get_all_fragments() {
    std::shared_ptr<std::vector<fragment>> data = std::make_shared<std::vector<fragment>>();
    snapshot_running = true;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wfor-loop-analysis"
    while (access_count > 0); //Spinlock
#pragma clang diagnostic pop
    fragment temp = {0, 0};
    while (fragments.unsynchronized_pop(std::ref(temp)))
        data->push_back(temp);
    for (auto &elem : *data)
        if (!fragments.unsynchronized_push(elem))
            throw "Something went wrong!";
    snapshot_running = false;
    return data;
}

int FragmentationManager::acquire_next_empty_fragment(int size) {
    fragment ret = {0, 0};
    if (fragments.pop(std::ref(ret))) {
        int pos = 0;
        if (ret.size >= size) {
            if (ret.size > size)
                fragments.push(fragment{ret.pos + size, ret.size - size});
            pos = ret.pos;
        } else {
            pos = acquire_next_empty_fragment(size);//TODO remove recursion
            fragments.push(ret);
        }
        write_trigger.notify_one();
        return pos;
    }
    return -1;
}

void FragmentationManager::fiber_run(path file) {
    FiberPool::acknowledge_new_fiber();
    auto l = Logger::get_logger("FragmentManager Fiber: " + file.filename().string());
    BOOST_LOG_SEV(l, info) << "Fiber called!";
    while (true) {
        {
            std::unique_lock<mutex> lk(write_trigger_mutex);
            write_trigger.wait(lk);
        }

        if (running)
            boost::this_fiber::sleep_for(std::chrono::seconds(5));

        BOOST_LOG_SEV(l, info) << "Making snapshot...";
        std::shared_ptr<std::vector<fragment>> snapshot = this->get_all_fragments();

        BOOST_LOG_SEV(l, info) << "Writing to a file...";
        unsigned long size = snapshot->size();
        ofstream f(file, std::ios::binary);
        f.write((char *) &size, sizeof(size));
        for (unsigned long i = size; i > 0; --i)
            f << snapshot->operator[](i - 1);
        f.close();

        BOOST_LOG_SEV(l, info) << "File closed";
        if (!running)
            return;
    }
    FiberPool::acknowledge_fiber_dead();
}