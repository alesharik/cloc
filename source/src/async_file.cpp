#include <utility>
#include "async_file.h"
#include "fiber_pool.h"
#include <boost/filesystem/fstream.hpp>
#include <boost/fiber/operations.hpp>

AsyncFile::AsyncFile(boost::filesystem::path path) : path(std::move(path)), working(true) {}

AsyncFile::~AsyncFile() {
    close();
    reader->join();
    delete reader;
}

void AsyncFile::open(std::shared_ptr<boost::fibers::unbuffered_channel<file_part>> channel) {
    this->channel = std::move(channel);
    reader = new boost::fibers::fiber([this]() {
        FiberPool::acknowledge_new_fiber();
        boost::filesystem::ifstream stream(path, std::ios::binary);
        char *buf = new char[bufLength];
        long count = 0;
        while (working && (count = stream.readsome(buf, bufLength)) > 0) {
            char *toSend = new char[count];
            memcpy(toSend, buf, static_cast<size_t>(count));
            this->channel->push(file_part{static_cast<unsigned long>(count), toSend});
            boost::this_fiber::yield();
        }
        stream.close();
        this->channel->close();
        FiberPool::acknowledge_fiber_dead();
    });
}

void AsyncFile::close() {
    if (!working)
        return;
    working = false;
}
