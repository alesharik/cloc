#include "fiber_pool.h"
#include "logger.h"
#include <thread>
#include <memory>
#include <boost/fiber/mutex.hpp>
#include <boost/fiber/algo/work_stealing.hpp>
#include <boost/fiber/condition_variable.hpp>
#include <boost/fiber/fiber.hpp>
#include <boost/thread/barrier.hpp>
#include <stdexcept>
#include <chrono>
#include <thread>

using namespace boost::fibers;

FiberPoolManager FiberPool::manager;

void FiberPoolManager::start(unsigned int concurrency, unsigned int actualConcurrency) {
    if (main != nullptr)
        throw std::runtime_error("Fiber pool already started");
    fibers = 0;
    auto l = Logger::get_logger("FiberPoolManager");
    std::shared_ptr<boost::barrier> start_barrier = std::make_shared<boost::barrier>(concurrency + 1);
    main = new std::thread(
            [concurrency, start_barrier, actualConcurrency](std::vector<std::thread *> &workers, mutex &shutdown_mutex,
                                                            condition_variable_any &shutdown_trigger) {
                auto l = Logger::get_logger("FiberPoolManager Main Thread");
                use_scheduling_algorithm<algo::work_stealing>(actualConcurrency);
                for (int i = 0; i < concurrency - 1; ++i) {
                    const int id = i + 1;
                    auto thr = new std::thread(
                            [start_barrier, concurrency, workers, &shutdown_mutex, &shutdown_trigger, actualConcurrency, id]() {
                                auto l = Logger::get_logger("FiberPoolManager Thread " + std::to_string(id));
                                use_scheduling_algorithm<algo::work_stealing>(actualConcurrency);
                                BOOST_LOG_SEV(l, debug) << "Thread starting";
                                start_barrier->wait();
                                BOOST_LOG_SEV(l, debug) << "Thread started";
                                shutdown_mutex.lock();
                                shutdown_trigger.wait(shutdown_mutex);
                                shutdown_mutex.unlock();
                                BOOST_LOG_SEV(l, debug) << "Thread shut down";
                            });
                    workers.push_back(thr);
                }
                BOOST_LOG_SEV(l, debug) << "Thread starting";
                start_barrier->wait();
                BOOST_LOG_SEV(l, debug) << "Thread started";
                shutdown_mutex.lock();
                shutdown_trigger.wait(shutdown_mutex);
                shutdown_mutex.unlock();
                BOOST_LOG_SEV(l, debug) << "Thread shut down";
            }, std::ref(workers), std::ref(shutdown_mutex), std::ref(shutdown_trigger));
    start_barrier->wait();
    BOOST_LOG_SEV(l, info) << "Pool started";
}

void FiberPoolManager::shutdown() {
    if (main == nullptr)
        throw std::runtime_error("Fiber pool is not started");
    auto l = Logger::get_logger("FiberPoolManager");
    std::thread([this]() {
        int i = 0;
        auto l = Logger::get_logger("FiberPoolManager Shutdown thread");
        while (fibers > 0) {
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
            if (++i > 5000) {
                BOOST_LOG_SEV(l, warning) << "Cannot shutdown pool because there are " << fibers
                                          << " running tasks. Please, Ctrl+C program";
                break;
            }
        }
    }).detach();
    shutdown_trigger.notify_all();
    //DO NOT JOIN WORKERS
    workers.clear();
    main = nullptr;
    BOOST_LOG_SEV(l, info) << "Pool shut down";
}

FiberPoolManager::~FiberPoolManager() {
    if (main != nullptr)
        shutdown();
}

void FiberPool::start() {
    unsigned int concurrency = std::thread::hardware_concurrency();
    if (concurrency < 2)//Reserve hardware thread for main thread(which is a worker)
        return;
    manager.start(concurrency - 1, concurrency);
}

void FiberPool::start(unsigned int concurrency) {
    manager.start(concurrency, concurrency);
}

void FiberPool::shutdown() {
    manager.shutdown();
}

