#ifndef CLOC_LINE_COUNTER_ALGO_H
#define CLOC_LINE_COUNTER_ALGO_H

#include "statistics.h"
#include <string>

class LineCounterAlgorithm {
public:
    virtual FileStatistics count(std::string string) = 0;
};

#endif //CLOC_LINE_COUNTER_ALGO_H
