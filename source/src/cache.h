#ifndef CLOC_CACHE_H
#define CLOC_CACHE_H

#include <string>
#include <boost/date_time.hpp>
#include <boost/filesystem.hpp>
#include "statistics.h"

using namespace boost::filesystem;
using namespace std;
using namespace boost::posix_time;

class Cache {
public:
    virtual ~Cache();

    /// This method resolves some concurrency problems by allowing cache to prepare it's structs for the actual data before
    /// thread tries to access the resource. This means that all put* method ideally must NOT have any write races besides
    /// the situation when 2 or more threads tries to write same data part to the same path, which won't happen in any case
    /// \param path the path to prepare
    virtual void prepare(path path) = 0;

    virtual bool contains(path path) = 0;

    virtual string get_checksum(path path) = 0;

    virtual ptime get_change_time(path path) = 0;

    virtual void put_checksum(path path, string checksum) = 0;

    virtual void put_change_time(path path, ptime time) = 0;

    virtual void put_file_stats(path path, FileStatistics stats) = 0;

    virtual FileStatistics get_file_stats(path path) = 0;
};

class CacheStub : public Cache {
public:
    ~CacheStub() override;

    bool contains(path path) override;

    string get_checksum(path path) override;

    void put_checksum(path path, string checksum) override;

    void put_change_time(path path, ptime time) override;

    void put_file_stats(path path, FileStatistics stats) override;

    ptime get_change_time(path path) override;

    FileStatistics get_file_stats(path path) override;

    void prepare(path path) override;
};


#endif //CLOC_CACHE_H
