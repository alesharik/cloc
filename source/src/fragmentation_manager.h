#ifndef CLOC_FRAGMENTATION_MANAGER_H
#define CLOC_FRAGMENTATION_MANAGER_H

#include <memory>
#include <vector>
#include <iostream>
#include <boost/atomic.hpp>
#include <boost/filesystem/path.hpp>
#include <boost/lockfree/stack.hpp>
#include <boost/fiber/condition_variable.hpp>
#include <boost/fiber/mutex.hpp>

using namespace boost::filesystem;
using namespace boost::fibers;

struct fragment {
    int pos;
    int size;
};

std::istream &operator>>(std::istream &stream, fragment &frag);

std::ostream &operator<<(std::ostream &stream, const fragment &frag);

class FragmentationManager {
    boost::atomic_bool running;
    boost::fibers::condition_variable write_trigger;
    boost::fibers::mutex write_trigger_mutex;
    boost::atomic_int access_count;
    boost::atomic_bool snapshot_running;
    boost::lockfree::stack<fragment> fragments;
    fiber *save_fiber;
public:
    explicit FragmentationManager(const path &file);

    FragmentationManager(FragmentationManager const &) = delete;

    FragmentationManager &operator=(FragmentationManager const &) = delete;

    virtual ~FragmentationManager();

    int acquire_next_empty_fragment(int size);

    std::shared_ptr<std::vector<fragment>> get_all_fragments();

    void add_empty_fragment(int fragment, int size);

private:
    void fiber_run(path file);
};

#endif //CLOC_FRAGMENTATION_MANAGER_H
