#ifndef CLOC_CPP_LINE_COUNTER_ALGORITHM_H
#define CLOC_CPP_LINE_COUNTER_ALGORITHM_H

#include "line_counter_algorithm.h"
#include <string>

namespace {
    enum block {
        NONE,
        FUNCTION,
        STRUCT,
        BLOCK
    };

    struct state {
        enum block block;
        std::string buffer = "";
    };
}

class CppLineCounterAlgorithm : public LineCounterAlgorithm {
public:
    FileStatistics count(std::string string) override;
};

#endif //CLOC_CPP_LINE_COUNTER_ALGORITHM_H
