#ifndef CLOC_HASHING_H
#define CLOC_HASHING_H

#include <iostream>
#include <string>

#ifdef OpenSSL_FOUND
#define SUPPORTED_ALGORITHMS_STRING "crc32, md5"
#else
#define SUPPORTED_ALGORITHMS_STRING "crc32"
#endif

enum HashingAlgorithm {
#ifdef OpenSSL_FOUND
    MD5,
#endif
    CRC32
};

std::istream &operator>>(std::istream &in, HashingAlgorithm &algorithm);

class Hash {
public:
    virtual ~Hash() = default;

    virtual void init() = 0;

    virtual void update(const char *data, unsigned int length) = 0;

    virtual std::string final() = 0;
};

class HashDeleter {
public:
    void operator()(Hash *) const;
};

#ifdef OpenSSL_FOUND

#include <openssl/evp.h>

class MD5Hash : public Hash {
    EVP_MD_CTX *ctx;
public:
    MD5Hash();

    ~MD5Hash() override;

    void init() override;

    void update(const char *data, unsigned int length) override;

    std::string final() override;
};

#endif

#ifdef ZLIB_FOUND

#include <zlib.h>

class CRC32Hash : public Hash {
    uLong result;

public:
    CRC32Hash();

    void init() override;

    void update(const char *data, unsigned int length) override;

    std::string final() override;
};

#else

#include <boost/crc.hpp>

class CRC32Hash : public Hash {
    boost::crc_32_type crc;
public:
    void init() override;

    void update(const char *data, unsigned int length) override;

    std::string final() override;
};

#endif

class Hasher {
    HashingAlgorithm algorithm;
public:
    explicit Hasher(HashingAlgorithm algorithm);

    Hash *newHash();
};

#endif //CLOC_HASHING_H
