cmake_minimum_required(VERSION 3.13)
project(cloc)

set(CMAKE_CXX_STANDARD 17)
enable_testing()
list(APPEND CMAKE_MODULE_PATH "${PROJECT_SOURCE_DIR}/")

add_subdirectory(source)
add_subdirectory(test)

add_test(NAME MyTest COMMAND Test)