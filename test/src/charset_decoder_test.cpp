#include <boost/test/unit_test.hpp>
#include "charset_decoder.h"

BOOST_AUTO_TEST_SUITE(CharsetDecoderTest)

    BOOST_AUTO_TEST_CASE(CanParseAscii) {
        CharsetDecoder::init("ru");
        const char *data = "test";
        BOOST_CHECK_NE("", CharsetDecoder::decode(data, 4, false));
    }

    BOOST_AUTO_TEST_CASE(CanParseRussian) {
        CharsetDecoder::init("ru");
        const char *data = "тест";
        BOOST_CHECK_NE("", CharsetDecoder::decode(data, 4, false));
    }

BOOST_AUTO_TEST_SUITE_END()