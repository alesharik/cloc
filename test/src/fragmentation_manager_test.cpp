#include <boost/test/unit_test.hpp>
#include "fragmentation_manager.h"
#include "fiber_pool.h"
#include "logger.h"
#include <boost/filesystem.hpp>
#include <boost/fiber/all.hpp>
#include <thread>
#include <chrono>

BOOST_AUTO_TEST_SUITE(FragmentationManagerTest)

    BOOST_AUTO_TEST_CASE(Working) {
        FiberPool::start(1);
        auto file = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        auto manager = new FragmentationManager(file);
        std::this_thread::sleep_for(std::chrono::milliseconds(3000));

        BOOST_CHECK_EQUAL(-1, manager->acquire_next_empty_fragment(1));
        BOOST_CHECK_EQUAL(0, manager->get_all_fragments()->size());

        manager->add_empty_fragment(1, 1);
        BOOST_CHECK_EQUAL(1, manager->get_all_fragments()->size());
        BOOST_CHECK_EQUAL(1, manager->acquire_next_empty_fragment(1));
        BOOST_CHECK_EQUAL(-1, manager->acquire_next_empty_fragment(1));
        BOOST_CHECK_EQUAL(0, manager->get_all_fragments()->size());

        delete manager;
        FiberPool::shutdown();
    }

    BOOST_AUTO_TEST_CASE(FileIOWorking) {
        FiberPool::start(1);
        auto file = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        auto manager = new FragmentationManager(file);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        manager->add_empty_fragment(1, 1);
        manager->add_empty_fragment(2, 1);
        manager->add_empty_fragment(3, 1);
        delete manager;
        FiberPool::shutdown();
        FiberPool::start(1);

        auto manager1 = new FragmentationManager(file);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        BOOST_CHECK_EQUAL(3, manager1->acquire_next_empty_fragment(1));
        BOOST_CHECK_EQUAL(2, manager1->acquire_next_empty_fragment(1));
        BOOST_CHECK_EQUAL(1, manager1->acquire_next_empty_fragment(1));

        delete manager1;
        FiberPool::shutdown();
        FiberPool::start(1);

        auto manager2 = new FragmentationManager(file);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        BOOST_CHECK_EQUAL(0, manager2->get_all_fragments()->size());
        delete manager2;
        FiberPool::shutdown();
    }

    BOOST_AUTO_TEST_CASE(LotsOfElements) {
        FiberPool::start(1);
        auto file = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        auto manager = new FragmentationManager(file);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        for (int j = 0; j < 1024; ++j) {
            manager->add_empty_fragment(j, 1);
        }
        for (int k = 1024 - 1; k <= 0; --k) {
            BOOST_CHECK_EQUAL(k, manager->acquire_next_empty_fragment(1));
        }

        delete manager;
        FiberPool::shutdown();
    }

    BOOST_AUTO_TEST_CASE(DifferentSize) {
        FiberPool::start(1);
        auto file = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        auto manager = new FragmentationManager(file);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        manager->add_empty_fragment(1000, 30);
        manager->add_empty_fragment(10, 1);
        BOOST_CHECK_EQUAL(1000, manager->acquire_next_empty_fragment(10));
        BOOST_CHECK_EQUAL(10, manager->acquire_next_empty_fragment(1));
        BOOST_CHECK_EQUAL(1010, manager->acquire_next_empty_fragment(10));
        BOOST_CHECK_EQUAL(-1, manager->acquire_next_empty_fragment(20));
        BOOST_CHECK_EQUAL(1020, manager->acquire_next_empty_fragment(5));
        BOOST_CHECK_EQUAL(1025, manager->acquire_next_empty_fragment(5));
        BOOST_CHECK_EQUAL(-1, manager->acquire_next_empty_fragment(1));
        delete manager;
        FiberPool::shutdown();
    }

BOOST_AUTO_TEST_SUITE_END()
