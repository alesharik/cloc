#include <boost/test/unit_test.hpp>
#include <ostream>
#include "cacheimpl.h"
#include "fiber_pool.h"
#include "logger.h"
#include <boost/filesystem.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/filesystem/fstream.hpp>

using namespace boost::posix_time;
using namespace boost::gregorian;

BOOST_AUTO_TEST_SUITE(CacheImplTest)

    BOOST_AUTO_TEST_CASE(Working) {
        FiberPool::start(1);
        auto rootDir = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        auto cacheDir = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        boost::filesystem::create_directories(rootDir);
        boost::filesystem::create_directories(cacheDir);
        CacheImpl *cache = new CacheImpl(rootDir, cacheDir);

        auto a = rootDir / boost::filesystem::unique_path();
        auto bcDir = rootDir / boost::filesystem::unique_path();
        auto b = bcDir / boost::filesystem::unique_path();
        auto c = bcDir / boost::filesystem::unique_path();

        BOOST_CHECK(!cache->contains(a));
        BOOST_CHECK(!cache->contains(b));
        BOOST_CHECK(!cache->contains(c));

        ptime aTime(date(2002, Jan, 10), time_duration(10, 20, 30));
        ptime bTime(date(2003, Jan, 10), time_duration(10, 20, 30));
        cache->prepare(a);
        cache->prepare(b);
        cache->prepare(c);
        cache->put_file_stats(a, FileStatistics(4, 1, 2, 1));
        cache->put_checksum(a, "a");
        cache->put_change_time(a, aTime);
        cache->put_file_stats(b, FileStatistics(10, 10, 0, 0));
        cache->put_checksum(b, "b");
        cache->put_change_time(b, bTime);

        BOOST_CHECK(cache->contains(a));
        BOOST_CHECK(cache->contains(b));
        BOOST_CHECK(!cache->contains(c));

        BOOST_CHECK_EQUAL("a", cache->get_checksum(a));
        BOOST_CHECK_EQUAL("b", cache->get_checksum(b));
        BOOST_CHECK_EQUAL(aTime, cache->get_change_time(a));
        BOOST_CHECK_EQUAL(bTime, cache->get_change_time(b));
        BOOST_CHECK_EQUAL(FileStatistics(4, 1, 2, 1), cache->get_file_stats(a));
        BOOST_CHECK_EQUAL(FileStatistics(10, 10, 0, 0), cache->get_file_stats(b));

        cache->put_file_stats(c, FileStatistics(11, 10, 1, 0));
        cache->put_checksum(c, "c");
        cache->put_change_time(c, aTime);

        BOOST_CHECK(cache->contains(a));
        BOOST_CHECK(cache->contains(b));
        BOOST_CHECK(cache->contains(c));

        BOOST_CHECK_EQUAL("a", cache->get_checksum(a));
        BOOST_CHECK_EQUAL("b", cache->get_checksum(b));
        BOOST_CHECK_EQUAL("c", cache->get_checksum(c));
        BOOST_CHECK_EQUAL(aTime, cache->get_change_time(a));
        BOOST_CHECK_EQUAL(bTime, cache->get_change_time(b));
        BOOST_CHECK_EQUAL(aTime, cache->get_change_time(c));
        BOOST_CHECK_EQUAL(FileStatistics(4, 1, 2, 1), cache->get_file_stats(a));
        BOOST_CHECK_EQUAL(FileStatistics(10, 10, 0, 0), cache->get_file_stats(b));
        BOOST_CHECK_EQUAL(FileStatistics(11, 10, 1, 0), cache->get_file_stats(c));

        delete cache;
        FiberPool::shutdown();
    }

    BOOST_AUTO_TEST_CASE(FileIOAutoCleanup) {
        FiberPool::start(1);
        auto rootDir = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        auto cacheDir = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        boost::filesystem::create_directories(rootDir);
        boost::filesystem::create_directories(cacheDir);
        CacheImpl *cache = new CacheImpl(rootDir, cacheDir);

        auto a = rootDir / boost::filesystem::unique_path();
        auto bcDir = rootDir / boost::filesystem::unique_path();
        auto b = bcDir / boost::filesystem::unique_path();
        auto c = bcDir / boost::filesystem::unique_path();

        ptime aTime(date(2002, Jan, 10), time_duration(10, 20, 30));
        ptime bTime(date(2003, Jan, 10), time_duration(10, 20, 30));
        cache->prepare(a);
        cache->prepare(b);
        cache->prepare(c);
        cache->put_file_stats(a, FileStatistics(4, 1, 2, 1));
        cache->put_checksum(a, "a");
        cache->put_change_time(a, aTime);
        cache->put_file_stats(b, FileStatistics(10, 10, 0, 0));
        cache->put_checksum(b, "b");
        cache->put_change_time(b, bTime);
        cache->put_file_stats(c, FileStatistics(11, 10, 1, 0));
        cache->put_checksum(c, "c");
        cache->put_change_time(c, aTime);

        BOOST_CHECK(cache->contains(a));
        BOOST_CHECK(cache->contains(b));
        BOOST_CHECK(cache->contains(c));
        delete cache;
        FiberPool::shutdown();
        FiberPool::start(1);
        CacheImpl *cache1 = new CacheImpl(rootDir, cacheDir);

        BOOST_CHECK(!cache->contains(a));
        BOOST_CHECK(!cache->contains(b));
        BOOST_CHECK(!cache->contains(c));

        delete cache1;
        FiberPool::shutdown();
    }

    BOOST_AUTO_TEST_CASE(FileIO) {
        FiberPool::start(1);
        auto rootDir = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        auto cacheDir = boost::filesystem::temp_directory_path() / boost::filesystem::unique_path();
        boost::filesystem::create_directories(rootDir);
        boost::filesystem::create_directories(cacheDir);
        CacheImpl *cache = new CacheImpl(rootDir, cacheDir);

        auto a = rootDir / boost::filesystem::unique_path();
        auto bcDir = rootDir / boost::filesystem::unique_path();
        boost::filesystem::create_directories(bcDir);
        auto b = bcDir / boost::filesystem::unique_path();
        auto c = bcDir / boost::filesystem::unique_path();
        boost::filesystem::ofstream aStream(a);
        aStream.close();
        boost::filesystem::ofstream bStream(b);
        bStream.close();
        boost::filesystem::ofstream cStream(c);
        cStream.close();

        ptime aTime(date(2002, Jan, 10), time_duration(10, 20, 30));
        ptime bTime(date(2003, Jan, 10), time_duration(10, 20, 30));
        cache->prepare(a);
        cache->prepare(b);
        cache->prepare(c);
        cache->put_file_stats(a, FileStatistics(4, 1, 2, 1));
        cache->put_checksum(a, "a");
        cache->put_change_time(a, aTime);
        cache->put_file_stats(b, FileStatistics(10, 10, 0, 0));
        cache->put_checksum(b, "b");
        cache->put_change_time(b, bTime);
        cache->put_file_stats(c, FileStatistics(11, 10, 1, 0));
        cache->put_checksum(c, "c");
        cache->put_change_time(c, aTime);

        delete cache;
        FiberPool::shutdown();

        FiberPool::start(1);
        CacheImpl *cache1 = new CacheImpl(rootDir, cacheDir);
        BOOST_CHECK(cache1->contains(a));
        BOOST_CHECK(cache1->contains(b));
        BOOST_CHECK(cache1->contains(c));

        BOOST_CHECK_EQUAL("a", cache1->get_checksum(a));
        BOOST_CHECK_EQUAL("b", cache1->get_checksum(b));
        BOOST_CHECK_EQUAL("c", cache1->get_checksum(c));
        BOOST_CHECK_EQUAL(aTime, cache1->get_change_time(a));
        BOOST_CHECK_EQUAL(bTime, cache1->get_change_time(b));
        BOOST_CHECK_EQUAL(aTime, cache1->get_change_time(c));
        BOOST_CHECK_EQUAL(FileStatistics(4, 1, 2, 1), cache1->get_file_stats(a));
        BOOST_CHECK_EQUAL(FileStatistics(10, 10, 0, 0), cache1->get_file_stats(b));
        BOOST_CHECK_EQUAL(FileStatistics(11, 10, 1, 0), cache1->get_file_stats(c));

        cache->put_file_stats(c, FileStatistics(100, 50, 49, 1));

        delete cache1;
        FiberPool::shutdown();

        FiberPool::start(1);
        CacheImpl *cache2 = new CacheImpl(rootDir, cacheDir);

        BOOST_CHECK(cache2->contains(a));
        BOOST_CHECK(cache2->contains(b));
        BOOST_CHECK(cache2->contains(c));

        BOOST_CHECK_EQUAL("a", cache2->get_checksum(a));
        BOOST_CHECK_EQUAL("b", cache2->get_checksum(b));
        BOOST_CHECK_EQUAL("c", cache2->get_checksum(c));
        BOOST_CHECK_EQUAL(aTime, cache2->get_change_time(a));
        BOOST_CHECK_EQUAL(bTime, cache2->get_change_time(b));
        BOOST_CHECK_EQUAL(aTime, cache2->get_change_time(c));
        BOOST_CHECK_EQUAL(FileStatistics(4, 1, 2, 1), cache2->get_file_stats(a));
        BOOST_CHECK_EQUAL(FileStatistics(10, 10, 0, 0), cache2->get_file_stats(b));
        BOOST_CHECK_EQUAL(FileStatistics(100, 50, 49, 1), cache2->get_file_stats(c));
        delete cache2;
        FiberPool::shutdown();
    }

BOOST_AUTO_TEST_SUITE_END()