#include <boost/test/unit_test.hpp>
#include "charset_converter.h"
#include <iconv.h>

BOOST_AUTO_TEST_SUITE(CharsetDecoderTest)

    BOOST_AUTO_TEST_CASE(ConvertAscii) {
        iconv_t conv = iconv_open("ASCII", "UTF-8");
        size_t length = 4;
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wwritable-strings"
        const char *in = "test";
        char *input = "test";
#pragma clang diagnostic pop
        char *out = new char[length];
        char *pout = out;
        size_t dstLen = length;
        iconv(conv, &input, &length, &pout, &dstLen);
        iconv_close(conv);

        BOOST_CHECK_EQUAL(in, CharsetConverter::to_utf8(out, 4, "ASCII"));
    }

BOOST_AUTO_TEST_SUITE_END()