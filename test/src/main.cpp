#define BOOST_TEST_MODULE "Tests for libcloc"

#include <boost/test/included/unit_test.hpp>
#include "cacheimpl_test.cpp"
#include "fragmentation_manager_test.cpp"
#include "charset_decoder_test.cpp"
#include "charset_converter_test.cpp"